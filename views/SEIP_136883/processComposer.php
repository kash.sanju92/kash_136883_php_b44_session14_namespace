<?php

require_once("../../vendor/autoload.php");



if (empty($_POST['StudentId']))
{
    echo "I am a Person <br>";

    $objPerson = new \App\Person();

    $objPerson->setName($_POST['UserName']);

    $objPerson->setDob($_POST['DateOfBirth']);

    echo $objPerson-> getName() . "<br>";

    echo $objPerson-> getDob() . "<br>";
}

else
{
    echo "I am a Student <br>";

    $objStudent = new \Tap\Student();

    $objStudent->setName($_POST['UserName']);

    $objStudent->setStdId($_POST['StudentId']);

    $objStudent->setDob($_POST['DateOfBirth']);

    echo $objStudent-> getName() . "<br>";

    echo $objStudent-> getDob() . "<br>";

    echo $objStudent-> getStdId() . "<br>";
}
